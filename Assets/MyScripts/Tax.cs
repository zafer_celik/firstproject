using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tax : MonoBehaviour
{
    int taxAmount;

    void OnTriggerEnter(Collider other)
    {
        taxAmount=Bank.instance.totalValue/10;
        Bank.instance.ChangeTotalValue(-taxAmount);
        Bank.instance.Withdrawal(taxAmount);
        MaleStripper.instance.StrippingMale();
        Destroy(this.gameObject);
    }
}
