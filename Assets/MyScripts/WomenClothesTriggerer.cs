using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A script to select the type of the object and the cost of it
/// </summary>
public enum Clothes
{
    cheapbra,
    cheapglasses,
    expensiveglasses,
    cheaphair,
    expensivehair,
    cheapupperdress,
    expensiveupperdress,
    cheappants,
    cheapskirts,
    expensiveskirts,
    cheapshoes,
    expensiveshoes,
    cheapbag,
    expensivebag
}
public class WomenClothesTriggerer : MonoBehaviour
{
    [System.Serializable]
    public class Properties
    {
        public int cost;
        public GameObject collectableObject;
    }

    [System.Serializable]
    public class Collectables
    {
        public Clothes clothes;
        //public CollectableAssigner.Pricer price;
        //public Properties properties;
    }
    public Collectables[] collectables;
    void Start()
    {
        //int numberss = Clothes.bra;
       // price = CollectableAssigner.instance.pricer;
        // collectables[0].properties.clothes
    }
    
    private void OnTriggerEnter(Collider other)
    {/*
        if (other.gameObject.tag=="Player")
        {

        }*/
    }
}
