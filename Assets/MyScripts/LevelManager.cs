using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Dreamteck.Splines;
using Animancer;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public static bool isGameStarted;
    public static bool isGameEnded;
    public static bool isShootingEnabled;
    public static int LevelNumber = 0;

    //AudioSource MainMenuAudioSource;
    public GameObject Player;
    public GameObject splineMap;
    public SplineFollower spFollower;
    public SplineComputer spComputer;
    [Header("Panels")]
    public GameObject LevelCompletedPanel;
    public GameObject IntroductionPanel, StartGamePanel, MoneyBar;

    public Slider slider;
    public Image image;
    public TextMeshProUGUI wealthName;
    public AnimancerComponent femaleAnimancer;

    public List<GameObject> Levels = new List<GameObject>();
    [SerializeField] int LevelIndex = 0;

    [SerializeField] float followSpeed = 7;

    public AnimationClip walk;
    public AnimationClip spin;
        
    void Awake()
    {
       
        if (instance == null)
        {
            instance = this;
            CreateLevel();
        }


    }
    void Start()
    {
       // HighScore = PlayerPrefs.GetInt("HighScore");

        //LevelText.text = "Level " + LevelNumber.ToString();
    }
    public void CreateLevel()
    {
        LevelIndex = PlayerPrefs.GetInt("LevelNo", 0);
        if (LevelIndex > Levels.Count - 1)
        {
            LevelIndex = 0;
            PlayerPrefs.GetInt("LevelNo", 0);
        }
        Instantiate(Levels[LevelIndex]);
        Levels[LevelIndex].SetActive(true);
        LevelNumber = LevelIndex + 1;

        splineMap = FindObjectOfType<SplineComputer>().gameObject;

        Player.GetComponent<Rigidbody>().useGravity = false;

        spFollower = Player.GetComponent<SplineFollower>();
        spComputer = splineMap.GetComponent<SplineComputer>();

        spFollower.spline = spComputer;

        spComputer.enabled = true;
        splineMap.GetComponent<MeshRenderer>().enabled = true;
        spComputer.gameObject.SetActive(true);

        femaleAnimancer = Player.GetComponent<AnimancerComponent>();
    }
    public void StartGame()
    {
        spFollower.enabled = true;


        splineMap.SetActive(true);
        Player.SetActive(true);

        StartGamePanel.SetActive(false);

        Player.GetComponent<Rigidbody>().useGravity = true;

        isGameStarted = true;
        isGameEnded = false;

        Player.GetComponent<Animator>().enabled = true;
        
        femaleAnimancer.enabled = true;
        Player.GetComponent<SplineFollower>().followSpeed = followSpeed;

        MoneyBar.SetActive(true);
        Bank.instance.SetInitialParameters(slider,image,wealthName);

        AnimationControls.instance.Walking();
    }
    //public void CallforWalk()
    //{
    //    AnimationControls.instance.Walking(femaleAnimancer,walk);
    //}
    //public void CallforSpin()
    //{
    //    AnimationControls.instance.CallforSpin(femaleAnimancer, spin);
    //}
    public void EndGame()
    {
        GameManager.instance.ChangeMaleStatus(false);
        isGameEnded = true;
        Invoke("StopPlayer", 1);
    }
    public void StopPlayer()
    {
        Player.GetComponent<SplineFollower>().followSpeed = 0;
    }
    public void Introductions()
    {
        StartGamePanel.SetActive(false);
        IntroductionPanel.SetActive(true);
    }
    public void ReturntoMainMenu()
    {
        StartGamePanel.SetActive(true);
        IntroductionPanel.SetActive(false);
    }
    public void OnLevelCompleted()
    {
        LevelCompletedPanel.SetActive(true);
    }
    public void OnLevelFailed()
    {
        //LevelFailedPanel.SetActive(true);
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        isGameStarted = false;
    }
    public void NextLevel()
    {
        LevelIndex++;
        PlayerPrefs.SetInt("LevelNo", LevelIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        isGameStarted = false;

    }
}
