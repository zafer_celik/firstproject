using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class AnimationControls : MonoBehaviour
{
    public static AnimationControls instance;
    [SerializeField] AnimancerComponent animancer;
    [SerializeField] AnimationClip walking;
    [SerializeField] AnimationClip spin;
    //[SerializeField] AnimationClip sadWalk;
    //[SerializeField] AnimationClip happyWalk;
    [SerializeField] AnimationClip dance;

    void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
    }
    void Start()
    {
        //Walking();
    }
    public void onEquiped()
    {
        var state = animancer.Play(spin, 0.3f); 
        state.Events.OnEnd = Walking;
        
        
    }

    public void Walking()
    {        
        animancer.Play(walking,0.35f);
    }
    public void Dance()
    {
        var state=animancer.Play(walking,0.3f);
        state.Events.OnEnd = () => { animancer.Play(dance, 0.3f); };
    }
    //public void CallforSpin(AnimancerComponent animancer,AnimationClip animation)
    //{
    //    spin = animation;
    //    var state = animancer.Play(walking, 1f);
    //    state.Events.OnEnd = () => { walking = null; animancer.Play(spin, 0.1f);Invoke("Rewalk", 0.5f); };
    //}
    //    public void Rewalk()
    //    {
    //        animancer.Stop(spin);
    //        LevelManager.instance.CallforWalk();
    //    }
}
