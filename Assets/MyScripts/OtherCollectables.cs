using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollectableObject
{
    sawx,
    sawy,
    tax,
    taxinverse,
}
public enum SawRotation
{
    X,
    Y,
    Z
}
public class OtherCollectables : MonoBehaviour
{

    public CollectableObject collectableObject;
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < GameManager.instance.objectProperties.Length; i++)
        {

            if (collectableObject==GameManager.instance.objectProperties[i].collectableObjects)
            {
                Instantiate(GameManager.instance.objectProperties[i].collectableObjectPrefab, this.transform.position, Quaternion.identity);
            }
        }
    }

}
