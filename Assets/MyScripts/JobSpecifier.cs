using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Professions
{
    Homeless,
    Firefighter,
    Policeman,
    Judge,
    Thief,
    Millionare,
    Unknown
}

public class JobSpecifier : MonoBehaviour
{
    public static JobSpecifier instance;
    [System.Serializable]
    public class Jobs
    {
        public Professions professions;
        public int salary;
        public GameObject Dress;
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }    
    }
    public Jobs[] jobs;
    //int FindAJob(Professions prof)
    //{
    //    for (int i = 0; i < jobs.Length; i++)
    //    {
    //        if(jobs[i].professions==prof)
    //        {
    //            return jobs[i].salary;
    //        }
    //    }
    //    return 0;
    //}
}
