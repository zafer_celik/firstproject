using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoneyObj
{
    [SerializeField] public GameObject Money;
}


public class MoneyPooler : MonoBehaviour
{
    [SerializeField] List<MoneyObj> moneyPool = new List<MoneyObj>();
    //[SerializeField] int moneyPoolCount = 20;
    [SerializeField] GameObject moneyPrefab;
    [SerializeField] GameObject moneyListParent;

    [SerializeField] int maxMoneyCount = 20000;
    [SerializeField] float moneyHeight = 0.09f;

    public int moneyNumber;


    public static MoneyPooler instance;

    private void OnEnable()
    {
        if (instance == null)
            instance = this;
        CreatePool();
    }

    void CreatePool()
    {
        for (int i = 0; i < maxMoneyCount / 1000; i++)
        {
            var obj = Instantiate(moneyPrefab, moneyListParent.transform);
            MoneyObj moneyObj = new MoneyObj();
            moneyObj.Money = obj;
            moneyObj.Money.transform.localPosition = CalculatePosition(i);
            moneyPool.Add(moneyObj);
            moneyObj.Money.SetActive(false);
        }
    }



    /// <summary>
    /// Calculating position of money
    /// </summary>
    /// <param name="count"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    Vector3 CalculatePosition(float count)
    {

        Vector3 pos = Vector3.zero;

        pos.x = 0;
        pos.y = count * moneyHeight;
        return pos;
    }


    public void GetMoneyFromPool()
    {
        int lastIndex = lastIndexF();

        if(lastIndex<20)
        {
            moneyNumber = lastIndex + 1;
            moneyPool[lastIndex].Money.SetActive(true);
        }

    }

    int lastIndexF()
    {
        for (int i = 0; i < moneyPool.Count; i++)
        {
            if (moneyPool[i].Money.activeInHierarchy == false)
            {
                return i;
            } 
        }
        return 20;
    }

    public void AddMoneyToPool()
    {
        int lastIndex = lastIndexF()-1;
        moneyNumber = lastIndex;
        if (lastIndex != 0 &&GameManager.instance.Male.activeSelf&&lastIndex!=-1)
        {
            moneyPool[lastIndex].Money.SetActive(false);
        }
        else if(GameManager.instance.Male!=null)
        {            
            GameManager.instance.ChangeMaleStatus(false);
        }
    }
}
