using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Bank : MonoBehaviour
{
    public static Bank instance;
    [SerializeField] int StartingBalance = 150;
    [SerializeField] int CurrentBalance;
    public int totalValue;
    [SerializeField] int maxValue = 25000;

    [SerializeField] AnimationClip walking,sadWalk,happyWalk,spin;

    float maximumValue;

    [SerializeField] TextMeshProUGUI wealthName;
    [SerializeField] Slider slider;

    [SerializeField] Image fill;
    //[SerializeField] TextMeshProUGUI GoldText;
    int previousHairValue;

    EtiquetteUpdater[] etiquetteUpdaters;
    public int GetCurrentBalance { get { return CurrentBalance; } }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        CurrentBalance = StartingBalance;

       // GoldText.text = "Gold : " + CurrentBalance.ToString();
    }
    void Start()
    {
        maximumValue = maxValue;
        //CalculateInitialWorth();
    }
    public void CalculateInitialWorth()
    {
              for (int i = 0; i < GameManager.instance.bodyPositionValue.Length; i++)
        {
            totalValue += GameManager.instance.bodyPositionValue[i].value;
        }
        SetValue(totalValue);
        wealthName.text = "Sad";
        //AnimationControls.instance.AnimationPlayer(sadWalk, 1f);
    }
    public void ChangeTotalValue(int value )
    {

        totalValue += value;
        if(value<0)
        {
            Withdrawal(value);
        }       
        if(totalValue<0)
        { 
            totalValue = 0; 
        }
        CalculateWorth(value);

        etiquetteUpdaters = FindObjectsOfType<EtiquetteUpdater>();
        for (int i = 0; i < etiquetteUpdaters.Length; i++)
        {
            etiquetteUpdaters[i].UpdateColors();
        }
    }

    public void CalculateWorth(int value)
    {
        totalValue += value;
        if(totalValue<0)
        {
            totalValue = 0;
        }
        SetValue(totalValue);
        float ratio = totalValue / maximumValue;
        if ((totalValue / maximumValue)<0.15f)
        {
            wealthName.text = "Sad";
            fill.color = Color.red;
            //AnimationControls.instance.AnimationPlayer(sadWalk, 1f);
        }
        else if(0.15f <= (totalValue / maximumValue)&& (totalValue / maximumValue)<0.35f)
        {
            wealthName.text = "Poor";
            fill.color = Color.gray;
            //AnimationControls.instance.AnimationPlayer(sadWalk, 1f);
        }
        else if (0.35f <= (totalValue / maximumValue) && (totalValue / maximumValue) < 0.6f)
        {
            wealthName.text = "Decent";
            fill.color = Color.green;
           // AnimationControls.instance.AnimationPlayer(happyWalk, 1f);
        }
        else if (0.6f <= (totalValue / maximumValue) && (totalValue / maximumValue) < 0.85f)
        {
            wealthName.text = "Rich";
            fill.color = Color.blue;
           // AnimationControls.instance.AnimationPlayer(happyWalk, 1f);
        }
        else if (0.85f <= (totalValue / maximumValue) && (totalValue / maximumValue) <= 1f)
        {
            wealthName.text = "Sugar";
            fill.color = Color.yellow;
            //AnimationControls.instance.AnimationPlayer(walking, 1f);
        }
        
    }
    public void Deposit(int amount)
    {
        CurrentBalance += Mathf.Abs(amount);
        //GoldText.text = "Gold : " + CurrentBalance.ToString();
    }
    public void Withdrawal(int amount)
    {
        CurrentBalance -= Mathf.Abs(amount);
        if (CurrentBalance < 0) { CurrentBalance = 0; }
        //GoldText.text = "Gold : " + CurrentBalance.ToString();
    }
    /// <summary>
    /// write a function that takes 2 variables which are old and new values of hair the function compares them ???
    /// </summary>
    /// <param name="hairvalue"></param>
    /// <returns></returns>
    public void SetMaxValue()
    {
        slider.maxValue = maxValue;
        slider.value = maxValue;
    }
    public void SetValue(int value)
    {
        slider.value = value;
    }
    public void SetInitialParameters(Slider sliderMoneyBar, Image image, TextMeshProUGUI wealthsname)
    {
        slider = sliderMoneyBar;
        fill = image;
        wealthName = wealthsname;
        SetMaxValue();
        CalculateInitialWorth();
    }
}
