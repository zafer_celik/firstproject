using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class MaleAnimationControls : MonoBehaviour
{
    [SerializeField] AnimancerComponent animancer;
    [SerializeField] AnimationClip idle;
    //[SerializeField] AnimationClip walking;

    void OnEnable()
    {
        Walking();
    }
    void Idle()
    {
        animancer.Play(idle);
    }
    
    void Walking()
    {
        //animancer.Play(walking);
    }
}
