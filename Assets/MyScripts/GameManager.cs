using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public enum DressingPosition
{
    Hair,
    Eyeglasses,
    upperbody,
    lowerbody,
    bag,
    feet,
}

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    public class CollactebleProperties
    {
        public Clothes clothes;
        public DressingPosition dressingPosition;
        public GameObject selectedObject;

    }
    [System.Serializable]
    public class PartofBody
    {
        public DressingPosition dressingPosition;
        public GameObject partsOfBody;

    }
    [System.Serializable]
    public class ClothActivator
    {
        public Clothes clothes;
        public DressingPosition dressingPosition;
        public int cost;
        public GameObject selectedObject;

    }
    [System.Serializable]
    public class BodyPositionValue
    {
        public DressingPosition dressingPosition;
        public int value;

    }
    [System.Serializable]
    public class MaleActivator
    {
        public Professions professions;
        public GameObject professionObject;
    }
    [System.Serializable]
    public class ObjectProperties
    {
        public CollectableObject collectableObjects;
        public GameObject collectableObjectPrefab;
    }
    int positionCounted;
    float bagHeight=1.5f;

    public CollactebleProperties[] collactebleProperties;
    public PartofBody[] partofBody;
    public ClothActivator[] clothActivator;
    public BodyPositionValue[] bodyPositionValue;
    public MaleActivator[] maleActivator;
    public ObjectProperties[] objectProperties;

    EtiquetteUpdater[] etiquetteUpdaters;

    public GameObject Male;
    public GameObject Mover;

    //public EtiquetteUpdater[] etiquetteUpdater;

    public static GameManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //CreateLevel();
        }
    }
    public void PathChanger()
    {
        if (Male.activeSelf&& Mover.GetComponent<PlayerMovementController>().pathWidth != 5.1f)
        {
            //float width = 6.5f;
            //width = Mathf.Lerp(6.5f, 5.1f, Time.deltaTime);
            Mover.GetComponent<PlayerMovementController>().pathWidth = 5.1f;
        }
        else if(!Male.activeSelf&& Mover.GetComponent<PlayerMovementController>().pathWidth != 6.5f)
        {
            //float width = 5.1f;
            //width = Mathf.Lerp(5.1f,6.5f, Time.deltaTime);
            Mover.GetComponent<PlayerMovementController>().pathWidth = 6.5f;
        }
    }
    public void ChangeMaleStatus(bool state)
    {
        Male.SetActive(state);
        PathChanger();
    }
    /// <summary>
    /// current situation is we created a void which takes 2 parameters as input which are type of the ofject and the objects transform
    /// the function creates the type 
    /// </summary>
    /// <param name="clothes"></param>
    /// <param name="objectsTransform"></param>
    public GameObject CollectableCreator(Clothes clothes, Transform objectsTransform)
    {
        for (int i = 0; i < collactebleProperties.Length; i++)
        {
            if (clothes == collactebleProperties[i].clothes)
            {
                if(clothes==Clothes.expensivebag||clothes==Clothes.cheapbag)
                {
                    objectsTransform.position += new Vector3(0, bagHeight, 0);
                }
                GameObject instantiatedObject=Instantiate(collactebleProperties[i].selectedObject, objectsTransform.position, Quaternion.identity);
                return instantiatedObject;
            }
        }
        return null;
    }
    public bool CollectableEquiper(Clothes clothes)
    {        
        for (int i = 0; i < clothActivator.Length; i++)
        {
            if (clothes == clothActivator[i].clothes)
            {
                if (Bank.instance.GetCurrentBalance >= clothActivator[i].cost)
                {
                    Bank.instance.Withdrawal(clothActivator[i].cost);
                    Bank.instance.CalculateWorth(clothActivator[i].cost);
                    MaleStripper.instance.StrippingMale();
                    AnimationControls.instance.onEquiped();

                    etiquetteUpdaters = FindObjectsOfType<EtiquetteUpdater>();
                    for (int n = 0; n < etiquetteUpdaters.Length; n++)
                    {
                        etiquetteUpdaters[n].UpdateColors();
                    }

                    //LevelManager.instance.CallforSpin();

                    if (Mathf.RoundToInt(clothActivator[i].cost/1000)>=1)
                    {
                        for (int m = 0; m < Mathf.RoundToInt(clothActivator[i].cost / 1000); m++)
                        {
                            MoneyPooler.instance.AddMoneyToPool();
                        }
                    }
                    
                    for (int l = 0; l < bodyPositionValue.Length; l++)
                    {
                        if(bodyPositionValue[l].dressingPosition==clothActivator[i].dressingPosition)
                        {
                            bodyPositionValue[l].value = clothActivator[i].cost;
                        }
                    }
                    
                    for (int j = 0; j < partofBody.Length; j++)
                    {
                        if (clothActivator[i].dressingPosition == partofBody[j].dressingPosition)
                        {
                            positionCounted = partofBody[j].partsOfBody.GetComponentsInChildren<Transform>().GetLength(0) - 1;
                            if (positionCounted > 0)
                            {
                                for (int k = 0; k < partofBody[j].partsOfBody.transform.childCount; k++)
                                {
                                    Transform child = partofBody[j].partsOfBody.transform.GetChild(k);
                                    child.gameObject.SetActive(false);
                                }
                            }
                            clothActivator[i].selectedObject.SetActive(true);
                            return true;
                        }
                    }
                }
            }
            
        }
        return false;
        //dont forget the cost
        //when you pick up the object destroy it
        //i need to pass the instantiated object to collectable assigner script where i can kill it when its picked up

        //create a threshold where the total values of objects is counted as the highest and the character is called rich


        //there needs to be a bar which shows are total value of objects

        //Big Problem if a males set active becomes false when it is activated game breaks



        //EndgameDance is necessary
        //spin animation still does not work


        //withdrawal yaparken addmoneytopooldemek daha mant�kl�
    }
}
