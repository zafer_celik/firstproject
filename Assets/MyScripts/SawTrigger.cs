using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawTrigger : MonoBehaviour
{
    [SerializeField] int penaltyValue = 2000;
    void OnTriggerEnter(Collider other)
    {
        Bank.instance.ChangeTotalValue(-penaltyValue);
        for (int i = 0; i < penaltyValue / 1000; i++)
        {
            if(GameManager.instance.Male.activeSelf)
            {
                MoneyPooler.instance.AddMoneyToPool();
            }
            
        }
        if (penaltyValue > 0)
        {
            MaleStripper.instance.StrippingMale();
        }
        penaltyValue = 0;
    }
}
