using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DoorTriggerController : MonoBehaviour
{
    public Professions professions;
    public TMP_Text professionText;

    EtiquetteUpdater[] etiquetteUpdaters;

    [SerializeField] Transform maleTransform;
  
    int professionNumber;
    int salary;

    string professionName;
    void Start()
    {
        for (int i = 0; i < JobSpecifier.instance.jobs.Length; i++)
        {
            if (professions == (Professions)i)
            {
                professionNumber = i;
                salary = JobSpecifier.instance.jobs[professionNumber].salary;
            }
        }
        if ((Professions)professionNumber==Professions.Unknown)
        {
            salary = Random.Range(0, 20) * 1000;
        }
            switch(professions)
            {
                case Professions.Homeless:
                    professionName = "homeless";
                    break;
                case Professions.Firefighter:
                    professionName = "f�ref�ghter";
                    break;
                case Professions.Policeman:
                    professionName = "pol�ceman";
                    break;
                case Professions.Judge:
                    professionName = "judge";
                    break;
                case Professions.Thief:
                    professionName = "th�ef";
                    break;
                case Professions.Millionare:
                    professionName = "m�ll�onare";
                    break;
                case Professions.Unknown:
                    professionName = "?";
                    break;
            }
        professionText.text = professionName;

        Instantiate(JobSpecifier.instance.jobs[professionNumber].Dress, maleTransform.position,maleTransform.rotation);
    }

    void OnTriggerEnter(Collider other)
    {
        Bank.instance.Withdrawal(Bank.instance.GetCurrentBalance);
        Bank.instance.Deposit(salary);
        GameManager.instance.ChangeMaleStatus(true);
       
        MaleClassDetector();
        etiquetteUpdaters = FindObjectsOfType<EtiquetteUpdater>();
        for (int i = 0; i < etiquetteUpdaters.Length; i++)
        {
            etiquetteUpdaters[i].UpdateColors();
        }
    }
    void MaleClassDetector()
    {
        for (int i = 0; i < GameManager.instance.maleActivator.Length; i++)
        {
            if (professions == GameManager.instance.maleActivator[i].professions)
            {
                for (int j = 0; j < GameManager.instance.maleActivator.Length; j++)
                {
                    GameManager.instance.maleActivator[j].professionObject.SetActive(false);
                }
                GameManager.instance.maleActivator[i].professionObject.SetActive(true);
                MaleStripper.instance.SetStripper(GameManager.instance.maleActivator[i].professionObject);
            }            
        }
        if(salary/1000>MoneyPooler.instance.moneyNumber)
        {
            int length = salary / 1000 - MoneyPooler.instance.moneyNumber;

            for (int k = 0; k < length; k++)
            {

                MoneyPooler.instance.GetMoneyFromPool();
            }
        }
        else if(salary/1000<MoneyPooler.instance.moneyNumber)
        {
            int length = Mathf.Abs(salary / 1000 - MoneyPooler.instance.moneyNumber);
            for (int k = 0; k < length; k++)
            {
                MoneyPooler.instance.AddMoneyToPool();
            }
        }
    }
}
