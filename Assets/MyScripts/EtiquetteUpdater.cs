using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EtiquetteUpdater : MonoBehaviour
{
    [SerializeField] GameObject etiquette;
    Material etiquetteMaterial;
    DressingPosition dressingPosition;
    [SerializeField] TextMeshPro priceText;
    int valueOfObject;
    void Start()
    {
        etiquetteMaterial=etiquette.gameObject.GetComponent<MeshRenderer>().material;
        UpdateColors();
        priceText.text = "$" + valueOfObject.ToString();
    }
    void IdentifyObject()
    {
            for (int i = 0; i < GameManager.instance.clothActivator.Length; i++)
            {
                if (this.gameObject.GetComponent<CollectableAssigner>().clothes == GameManager.instance.clothActivator[i].clothes)
                {
                    valueOfObject = GameManager.instance.clothActivator[i].cost;
                    dressingPosition = GameManager.instance.clothActivator[i].dressingPosition;
                }
            }
        }
    public void UpdateColors()
    {
        IdentifyObject();
        for (int i = 0; i < GameManager.instance.bodyPositionValue.Length; i++)
        {
            if(dressingPosition==GameManager.instance.bodyPositionValue[i].dressingPosition)
            {
                if (valueOfObject <= Bank.instance.GetCurrentBalance)
                {
                    etiquetteMaterial.color = Color.green;
                }
                else if(valueOfObject >Bank.instance.GetCurrentBalance)
                {
                    etiquetteMaterial.color = Color.red;
                }
            }
        }
    }
}
