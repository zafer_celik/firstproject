using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class FinishTriggerer : MonoBehaviour
{
    [SerializeField] GameObject moneyRain,fireworks;
    void OnTriggerEnter(Collider other)
    {
        moneyRain.SetActive(true);
        fireworks.SetActive(true);
        LevelManager.instance.EndGame();
        Invoke("PlayerDancer", 1);
        Invoke("CompleteLevel", 4f);          
    }
    void PlayerDancer()
    {
        AnimationControls.instance.Dance();
    }
    void CompleteLevel()
    {
        LevelManager.instance.OnLevelCompleted();
        moneyRain.SetActive(false);
        fireworks.SetActive(false);
    }
}
