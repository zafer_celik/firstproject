using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Saw : MonoBehaviour
{
    [SerializeField] float duration = 2f;

    public SawRotation sawRotation;
    void Start()
    {
        StartCoroutine(MoveCoroutine());
    }

    IEnumerator MoveCoroutine()
    {
        Transform saw = transform.GetChild(0);
        while (true)
        {
            if (sawRotation==SawRotation.X)
            {
                saw.DOLocalRotate(new Vector3(360, 90, 0), duration * 2, RotateMode.FastBeyond360);

                saw.DOLocalMoveX(3, duration).SetEase(Ease.Flash);
                yield return new WaitForSeconds(duration);
                saw.DOLocalMoveX(-3.85f, duration).SetEase(Ease.Flash);

                yield return new WaitForSeconds(duration);
            }
            else if(sawRotation==SawRotation.Y)
            {
                saw.DOLocalRotate(new Vector3(360, 0, 0), duration * 2, RotateMode.FastBeyond360);

                saw.DOLocalMoveZ(3, duration).SetEase(Ease.Flash);
                yield return new WaitForSeconds(duration);
                saw.DOLocalMoveZ(-3.85f, duration).SetEase(Ease.Flash);

                yield return new WaitForSeconds(duration);
            }

        }
    }

}
