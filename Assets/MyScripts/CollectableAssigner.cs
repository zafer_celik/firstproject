using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EtiquetteUpdater))]
public class CollectableAssigner : MonoBehaviour
{
    public Clothes clothes;
    GameObject instantiatedObj;
    [SerializeField] Transform instantiationTransform;
    EtiquetteUpdater etiquetteUpdater;
    EtiquetteUpdater[] etiquetteUpdaters;

    bool isEquiped;
    void Start()
    {
        instantiatedObj=GameManager.instance.CollectableCreator(clothes, instantiationTransform);
        //etiquetteUpdater = GetComponent<EtiquetteUpdater>();
        etiquetteUpdaters=FindObjectsOfType<EtiquetteUpdater>();
    }
    void OnTriggerEnter(Collider other)
    {
        //etiquetteUpdaters = FindObjectsOfType<EtiquetteUpdater>();
        //for (int i = 0; i < etiquetteUpdaters.Length; i++)
        //{
        //    etiquetteUpdaters[i].UpdateColors();
        //}
        //BroadcastMessage("UpdateColors", SendMessageOptions.DontRequireReceiver);
        //etiquetteUpdater.UpdateColors();
        isEquiped=GameManager.instance.CollectableEquiper(clothes);
        if(isEquiped)
        {
            Destroy(instantiatedObj);
            Destroy(this.gameObject);
            AnimationControls.instance.onEquiped();
        }
      
    }
}
