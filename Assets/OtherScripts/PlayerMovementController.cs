using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public float pathWidth = 6f;
    [SerializeField] private float movementSpeed = 10f;
    //[SerializeField] private float rotationSpeed = 50f;
    private AInputManager _inputManager;
    private Rigidbody _rigidbody;
    
    private void Awake()
    {
        _inputManager = GetComponent<AInputManager>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Vector3 movePos = transform.localPosition;
        movePos.x += _inputManager.InputDirection.x;
        movePos.x = Mathf.Clamp(movePos.x, -pathWidth / 2f, pathWidth / 2f);
        transform.localPosition = Vector3.Lerp(transform.localPosition, movePos, Time.deltaTime * movementSpeed);
        
        //Quaternion lookRot = Quaternion.Euler(0f, _inputManager.InputDirection.x * rotationSpeed, 0f);
        //transform.localRotation = Quaternion.Slerp(transform.localRotation, lookRot, Time.deltaTime * rotationSpeed);
    }
}
